package com.examples;

import com.bemu.BEmu.*; //un-comment this line to use the Bloomberg API Emulator
//import com.bloomberglp.blpapi.*; //un-comment this line to use the actual Bloomberg API

public class RunReferenceDataRequest
{
	public static void RunExample() throws Exception
	{
		SessionOptions soptions = new SessionOptions();
		soptions.setServerHost("127.0.0.1");
		soptions.setServerPort(8194);
		
		Session session = new Session(soptions);
		if(session.start() && session.openService("//blp/refdata"))
		{
            CorrelationID requestID = new CorrelationID(1);
			Service service = session.getService("//blp/refdata");
			Request request = service.createRequest("ReferenceDataRequest");

            //request information for the following securities
            request.append("securities", "TEST US EQUITY");

            //include the following simple fields in the result
            request.append("fields", "PX_LAST");
//            request.append("fields", "BID");
//            request.append("fields", "ASK");
            request.append("fields", "TICKER");
//            request.append("fields", "AMT_ISSUED");
//            request.append("fields", "ID_ISIN");
            session.sendRequest(request, requestID);

            boolean continueToLoop = true;
            while (continueToLoop)
            {
                Event eventObj = session.nextEvent();
                switch (eventObj.eventType().intValue())
                {
                    case Event.EventType.Constants.RESPONSE: // final event
                        continueToLoop = false;
                        handleResponseEvent(eventObj);
                        break;
                    case Event.EventType.Constants.PARTIAL_RESPONSE:
                        handleResponseEvent(eventObj);
                        break;
                }
            }
		}		
	}



    private static void handleResponseEvent(Event eventObj) throws Exception
    {
    	System.out.println("EventType =" + eventObj.eventType());
		MessageIterator miter = eventObj.messageIterator();
		while(miter.hasNext())
        {
			Message message = miter.next();
			
			Element elmSecurityDataArray = message.getElement("securityData");
            for (int valueIndex = 0; valueIndex < elmSecurityDataArray.numValues(); valueIndex++)
            {
                Element elmSecurityData = elmSecurityDataArray.getValueAsElement(valueIndex);

                String security = elmSecurityData.getElementAsString("security");
                System.out.println();
                System.out.println();
                System.out.println(security);

                Element elmFieldData = elmSecurityData.getElement("fieldData");
                double pxLast = elmFieldData.getElementAsFloat64("PX_LAST");
//              double bid = elmFieldData.getElementAsFloat64("BID");
//              double ask = elmFieldData.getElementAsFloat64("ASK");
                String ticker = elmFieldData.getElementAsString("TICKER");
//              double amt_issued = elmFieldData.getElementAsFloat64 ("AMT_ISSUED");
//              String id_isin = elmFieldData.getElementAsString("ID_ISIN");

                  System.out.println("PX_LAST = " + String.valueOf(pxLast));
//                System.out.println("BID = " + String.valueOf(bid));
//                System.out.println("ASK = " + String.valueOf(ask));
                System.out.println("TICKER = " + ticker);
//                System.out.println("ID_ISIN = " + id_isin);
//                System.out.println("AMT_ISSUED = " + String.valueOf(amt_issued));

            }
        }
    }

}
